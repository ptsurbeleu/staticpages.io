// custom extension for jQuery (https://stackoverflow.com/a/920322)
jQuery.fn.exists = function () {
  return this.length !== 0;
};

(function(global, $) {
  "use strict";
  // find typewriter's container
  var container = $("#pages_for"),
      fancy_comparable = function() { return 0.5 - Math.random() },
      pages_for = [
        "humans", "you", "him", "her", "us", "them", "projects",
        "bloggers", "hackers", "developers", "me", "programmers",
        "artists", "thinkers", "tinkerers", "startups",
        "ninjas", "marketers", "elves", "gamers", "moms",
        "ronins", "creators", "writers", "scientists", "ideas",
        "students", "teachers", "consultants", "makers", "dandies",
        "inventors", "engineers", "photographers", "dads",
        "players", "experiments", "fun", "profit", "charity",
        "education", "events", "occasions", "weddings", "conferences",
        "meetups", "communities", "hobbyists", "portfolios", "lawyers",
        "business", "hobby", "chefs", "restaraunts",
        "farmers", "cafes", "bistros", "shops", "bakeries", "doers",
        "dreamers", "enterpreneurs", "realtors", "humanity", "anyone",
        "anything", "ladies", "gentlemen", "citizens", "accountants",
        "musicians", "family", "teams",
        "athletes", "researchers", "enterprises", "companies", "catalogs",
        "marketing", "products", "apps", "services", "suppliers", "advisors",
        "blogs", "vlogs", "Rumpelstiltskin", "physicists", "laboratories",
        "clubs", "gyms", "readers", "producers",
      ].sort(fancy_comparable);

  // configure typewriter only when countainer has been found
  if (container.exists()) {
    var typewriter = new Typewriter(container[0], {
      loop: true,
      autoStart: true,
      cursor: "&#9612;",
      strings: pages_for,
      deleteSpeed: 50,
    });
    // activate TypewriterJS
    typewriter.start();
  }

  // port of email validation regex from Ruby on Rails - /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i
  // setup validation for the only form
  $("#subscribe-form").validate({
    // display errors only on form submission
    onkeyup: false,
    onfocusout: false,
    onclick: false,
    // customize error label container to display validation errors
    errorLabelContainer: "#omg-validation-errors",
    showErrors: function(errorMap, errorList) {
      // apply data-balloon-* attributes to the parent element
      for(var i in errorList) {
        // here is our error object
        var error = errorList[i];
        // obtain reference to the parent
        var parent = $(error.element).parent();
        // apply "data-balloon" attributes to display tooltip
        parent
          .attr("data-balloon", error.message)
          .attr("data-balloon-visible", ""); 
      }
    }
  });
})(this, jQuery);