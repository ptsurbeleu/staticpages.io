---
title: About
section: about
---
<!-- About section -->
<div role="tabpanel" class="tab-pane active" id="about">
    <div class="container about-area">
        <div class="row about-item">
            {% for hint in site.data.about %}
                {% assign mod = forloop.index | modulo: 3 %}
                <div class="icon col-sm-6 col-md-4">
                    <!-- Start: {{ hint.feature }} -->
                    <div class="icon_img">
                        <div class="hex-wrap">
                            <div class="hex yellow">
                                <i class="fa {{ hint.icon }} fa-4x"></i>
                            </div>
                        </div>
                        <div class="icon_detail">
                            <h3>{{ hint.feature }}</h3>
                            <p>{{ hint.summary }}</p>
                        </div>
                    </div>
                    <!-- End: {{ hint.feature }} -->
                </div>

                {% if mod == 0 %}
                <div class="col-sm-12 my-5 clearfix"></div>
                {% endif %}
            {% endfor %}
        </div>
    </div>
</div>