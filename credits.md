---
title: Credits
section: credits
---

<!-- Credits section -->
<div role="tabpanel" class="tab-pane active" id="credits">
    <div class="container credits-area">
        <div class="row credits-item">
            <h1>Thank You!</h1>
            {% for credit in site.data.credits %}
            <div class="icon col-sm-6 col-md-4 my-5">
                <div class="icon_img">
                    <div class="icon_detail">
                        <h3><a target="_blank" href="{{ credit.homepage }}">{{ credit.project }}</a></h3>
                        <p>{{ credit.highlights }}</p>
                    </div>
                </div>
            </div>
            {% endfor %}
        </div>
    </div>
</div>