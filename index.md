---
layout: default
title: Home
section: home
---

<!-- Home section -->
<div role="tabpanel" class="tab-pane active" id="home">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="animation-container">
                    <svg viewBox="0 205 600 600">
                        <symbol id="text">
                            <text text-anchor="middle" x="50%" y="50%">COMING SOON</text>
                        </symbol>
                        <use xlink:href="#text" class="text"></use>
                        <use xlink:href="#text" class="text"></use>
                        <use xlink:href="#text" class="text"></use>
                        <use xlink:href="#text" class="text"></use>
                    </svg>
                </div>
                <form class="subsribbe-box form-inline" id="subscribe-form" method="post" action="/rsvp">
                    <h1 class="mb-5">Web pages for <span id="pages_for">humans</span></h1>
                    <p class="pb-4">Join our mailing list for the latest updates and private beta invitation.</p>
                    <div class="form-group" data-balloon-pos="down" data-balloon-length="large">
                        <input type="email" class="form-control" name="email_address" placeholder="Email" required="true" />
                    </div>
                    <input type="submit" class="btn submit-btn" value="SUBSCRIBE" />
                </form>
            </div><!-- END OF /. COLUMN -->
        </div><!-- END OF /. ROW -->
    </div><!-- END OF /. CONTAINER FLUID -->
</div><!-- END OF /. HOME -->