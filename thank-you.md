---
layout: default
title: Thank You!
---

<div role="tabpanel" class="tab-pane active" id="credits">
    <div class="container about-area">
        <div class="row about-item">
            <h1 style="text-align: center; color: #fff; text-transform: uppercase;">Thank You!</h1>
        </div>
    </div>
</div>